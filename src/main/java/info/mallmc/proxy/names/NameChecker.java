/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.mallmc.proxy.names;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import info.mallmc.proxy.Proxy;
import info.mallmc.proxy.util.Messaging;
import java.util.ArrayList;
import org.mongodb.morphia.Datastore;

/**
 * @author Rushmead
 */
public class NameChecker {

  private static final Datastore database = Proxy.getInstance().getMallCore().getDatabase().getDatastore();
  private static final ArrayList<String> names = new ArrayList<>();

  public static ArrayList<String> getNames() {
    return names;
  }

  /**
   * Check if a user has already been warned about their name
   *
   * @param username The username to check
   * @return Whether or not the user has already been warned
   */
  private static boolean hasAlreadyWarned(String username) {
    return names.contains(username);
  }

  /**
   * Add a name warning for a specific username
   *
   * @param username The username to add a name warning for
   * @param name Word that is included in name.
   */
  private synchronized static void addNameWarning(String username, Names name) {
    if (name.getSeverity() == 0 || hasAlreadyWarned(username)) {
      return;
    }
    DBObject newWarning = new BasicDBObject("username", username);
    newWarning.put("severity", name.getSeverity());
    newWarning.put("solved", false);
    newWarning.put("date", System.currentTimeMillis() / 1000);
    database.getDB().getCollection("name_warnings").insert(newWarning);
    names.add(username);
    Messaging
        .sendStaffAlertMessage("{0} has been detected as a possible offense name(Level {1})", true, username,
            name.getSeverity());
  }

  /**
   * Reload the name warnings from the database
   */
  public synchronized static void reloadNameWarnings() {
    names.clear();
    DBCursor cursor = database.getDB().getCollection("name_warnings").find();
    while (cursor.hasNext()) {
      names.add((String) cursor.next().get("username"));
    }
  }

  /**
   * Check a username to see if it is offensive
   *
   * @param username The username to check.
   */
  public static void checkName(String username) {
    Names warning = Names.CLEAN;

    for (String word : Names.MILD.getWords()) {
      if (username.toLowerCase().contains((word.toLowerCase()))) {
        warning = Names.MILD;
      }
    }

    for (String word : Names.MEDIUM.getWords()) {
      if (username.toLowerCase().contains((word.toLowerCase()))) {
        warning = Names.MEDIUM;
      }
    }

    for (String word : Names.SEVERE.getWords()) {
      if (username.toLowerCase().contains((word.toLowerCase()))) {
        warning = Names.SEVERE;
      }
    }

    if (warning.getSeverity() > 0) {
      addNameWarning(username, warning);
    }
  }
}
