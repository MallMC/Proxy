package info.mallmc.proxy.events;

import info.mallmc.core.api.ChatChannel;
import info.mallmc.core.api.bans.Ban;
import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.core.api.player.PermissionSet;
import info.mallmc.core.database.BanDB;
import info.mallmc.core.database.PlayerData;
import info.mallmc.core.util.BanUtil;
import info.mallmc.proxy.Proxy;
import info.mallmc.proxy.names.NameChecker;
import info.mallmc.proxy.util.MOTDUtil;
import info.mallmc.proxy.util.Messaging;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.event.LoginEvent;
import net.md_5.bungee.api.event.PermissionCheckEvent;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.event.ProxyPingEvent;
import net.md_5.bungee.api.event.ServerKickEvent;
import net.md_5.bungee.api.event.ServerSwitchEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class PlayerEvents implements Listener {

  @EventHandler
  public void onPlayerLogin(LoginEvent event) {
    if(!Proxy.getInstance().isPublicMode()){
      PlayerData.getInstance().isInDatabase(event.getConnection().getUniqueId()).whenComplete((isInDatabase, throwable) -> {
        if(isInDatabase){
          PlayerData.getInstance().getPlayerByUUID(event.getConnection().getUniqueId()).whenComplete(((player, throwable1) -> {
            if(player.getPermissionSet().getPower() < PermissionSet.STAFF.getPower()){
              event.setCancelled(true);
              event.setCancelReason(Messaging.colorizeMessage(
                  Messaging.getPrefix(Messaging.COMMAND_SENDER_LANGUAGE_IDENTIFER) + "&aWe are coming soon... Please wait <3"));
            }
          }));
        }else{
          event.setCancelled(true);
          event.setCancelReason(Messaging.colorizeMessage(
              Messaging.getPrefix(Messaging.COMMAND_SENDER_LANGUAGE_IDENTIFER) + "&aWe are coming soon... Please wait <3"));
        }
      });
    }
    BanDB.getInstance().hasBanExpired(event.getConnection().getUniqueId()).whenComplete((hasExpired, throwable) -> {
      if(hasExpired){

        MallPlayer.createPlayer(event.getConnection().getUniqueId(), event.getConnection().getName());
      }else{
        Ban ban = BanDB.getInstance().getActiveBanFromUUID(event.getConnection().getUniqueId());
        event.setCancelReason(new TextComponent(TextComponent.fromLegacyText(BanUtil.getKickMessage(Long.parseLong(ban.getEndDate()), ban.getReason()))));
        event.setCancelled(true);
      }
    });
  }

  @EventHandler
  public void onPlayerJoin(PostLoginEvent event) {
    Proxy.getInstance().getProxy().getScheduler().runAsync(Proxy.getInstance(), () -> {
      ProxiedPlayer p = event.getPlayer();
      NameChecker.checkName(p.getName());
    });
  }

  @EventHandler
  public void onPlayerQuit(PlayerDisconnectEvent e) {
    for(String gameName : Proxy.getInstance().getGamesQueue().keySet()){
      if(Proxy.getInstance().getGamesQueue().get(gameName).contains(e.getPlayer().getUniqueId())){
        Proxy.getInstance().getGamesQueue().get(gameName).remove(e.getPlayer().getUniqueId());
      }
    }
    MallPlayer mallPlayer =
        MallPlayer.getPlayer(e.getPlayer().getUniqueId());
    mallPlayer.setPlayTime(mallPlayer.getPlayTime() + (System.currentTimeMillis() - mallPlayer.getLoginTime()));
    PlayerData.getInstance().savePlayer(MallPlayer.getPlayer(e.getPlayer().getUniqueId())).whenComplete((hasSaved, throwable) -> {
      MallPlayer.removePlayer(e.getPlayer().getUniqueId());
    });
  }

  @EventHandler
  public void onSwitchServer(ServerSwitchEvent event){
    MallPlayer.getPlayer(event.getPlayer().getUniqueId()).setLastServer(event.getPlayer().getServer().getInfo().getName());
  }

  @EventHandler
  public void onEventPerm(PermissionCheckEvent e) {
    if (e.getSender() instanceof ProxiedPlayer) {
      ProxiedPlayer p = (ProxiedPlayer) e.getSender();
      MallPlayer mp = MallPlayer.getPlayer(p.getUniqueId());
      if(e.getPermission().equals("discord")) {
        e.setHasPermission(true);
      }
      if (e.getPermission().equals("bungeecord.command.server")) {
        if (mp == null) {
          return;
        }
        if (mp.getPermissionSet().getPower() >= PermissionSet.STAFF.getPower()) {
          e.setHasPermission(true);
        }
      }
      if (mp.getPermissionSet().getPower() >= PermissionSet.ADMIN.getPower()) {
        e.setHasPermission(true);
      }
    }
  }

  @EventHandler
  public void onChat(ChatEvent event) {
    if (event.getMessage().startsWith("/")) {
      event.setCancelled(false);
      return;
    }
    if (event.getSender() instanceof ProxiedPlayer) {
      ProxiedPlayer proxiedPlayer = (ProxiedPlayer) event.getSender();
      MallPlayer mallPlayer = MallPlayer.getPlayer(proxiedPlayer.getUniqueId());

      switch (mallPlayer.getChatChannel()) {
        case ALL:
          event.setCancelled(false);
          break;
        case PARTY:
          break;
        case STAFF:
          if (mallPlayer.getPermissionSet().getPower() >= ChatChannel.STAFF.getPermissionSet().getPower()) {
            event.setCancelled(true);
            Messaging.sendStaffChatMessage(mallPlayer, event.getMessage(), true);
          } else {
            mallPlayer.setChatChannel(ChatChannel.ALL);
          }
          break;
        default:
          event.setCancelled(false);
          break;
      }
    }
  }

  @EventHandler
  public void onPlayerLogin(ServerKickEvent event) {
    if(event.getKickedFrom().getName().equalsIgnoreCase("hub1")){
      event.setCancelled(false);
      return;
    }
    event.setCancelServer(Proxy.getInstance().getProxy().getServerInfo("hub1"));
    event.setCancelled(true);
  }

  @EventHandler
  public void onMOTD(ProxyPingEvent e) {
    ServerPing ping = e.getResponse();
    ping.setDescriptionComponent(MOTDUtil.getMOTD());
    ping.getPlayers().setMax(Proxy.getInstance().getMaxPlayers());
    e.setResponse(ping);
  }

}
