package info.mallmc.proxy.events;

import info.mallmc.core.MallCore;
import info.mallmc.core.api.GameState;
import info.mallmc.core.api.Server;
import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.core.database.ServerData;
import info.mallmc.core.events.Event;
import info.mallmc.core.events.EventHandler;
import info.mallmc.core.events.EventType;
import info.mallmc.core.events.events.AddServerEvent;
import info.mallmc.core.events.events.GameStartEvent;
import info.mallmc.core.events.events.JoinGameEvent;
import info.mallmc.core.events.events.PlayerReadyEvent;
import info.mallmc.core.events.events.ServerReadyEvent;
import info.mallmc.core.events.events.discord.DiscordMessageReceivedEvent;
import info.mallmc.proxy.Proxy;
import info.mallmc.proxy.util.Messaging;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import net.md_5.bungee.api.config.ServerInfo;

public class CoreEvents {


  public static void handleCoreEvents() {
    EventHandler.registerEvent(EventType.JOIN_GAME_EVENT, CoreEvents::handleJoinGame);
    EventHandler.registerEvent(EventType.ADD_SERVER_EVENT, CoreEvents::addServer);
    EventHandler.registerEvent(EventType.GAME_START_EVENT, CoreEvents::handleGameStart);
    EventHandler.registerEvent(EventType.SERVER_READY_EVENT, CoreEvents::handleServerReady);
    EventHandler.registerEvent(EventType.PLAYER_READY, CoreEvents::playerReady);
    EventHandler.registerEvent(EventType.MESSAGE_FORM_DISCORD_EVENT, CoreEvents::onDiscordChatEvent);
  }

  private static void onDiscordChatEvent(Event event)
  {
    DiscordMessageReceivedEvent messageReceivedEvent = (DiscordMessageReceivedEvent)  event;
    Messaging.sendStaffChatMessage("[Discord] [" + messageReceivedEvent.getUsername() + "]: " + ((DiscordMessageReceivedEvent) event).getMessage());
  }

  private static void playerReady(Event event){
    PlayerReadyEvent playerReadyEvent = (PlayerReadyEvent) event;
    MallPlayer mp = playerReadyEvent.getPlayer();
    mp.setLoginTime(System.currentTimeMillis());
    if(mp.getFriendRequests() != null && mp.getFriendRequests().size() > 0){
      Messaging
          .sendClickableMessage(Proxy.getInstance().getProxy().getPlayer(mp.getUuid()), "proxy.friends.join", "/friend list", mp.getFriendRequests().size());
    }
  }


  private static void addServer(Event event){
    AddServerEvent addServerEvent = (AddServerEvent)event;
    Server server = ServerData.getInstance().getCurrentServer(addServerEvent.getServerIP(), addServerEvent.getServerPort());
    if(server ==null){
      return;
    }
    ServerInfo newServer = Proxy.getInstance().getProxy().constructServerInfo(server.getNickname(), new InetSocketAddress(server.getIp(), server.getPort()),"lolhax", false);
    Proxy.getInstance().getProxy().getServers().put(server.getNickname(), newServer);
  }

  private static void handleGameStart(Event event){
    GameStartEvent gameStartEvent = (GameStartEvent)event;

    final boolean[] foundServer = {false};
    final List<String> pingedServers = new ArrayList<>();
    Map<String, ServerInfo> serverInfoMap = Proxy.getInstance().getProxy().getServers();
    for (String name : serverInfoMap.keySet()){
      if(name.contains(gameStartEvent.getGameName())){
        if(gameStartEvent.getGameName().contains("laser")){
          Proxy.getInstance().getProxy().getServers().get(name).ping((result, error) -> {
            if(error!=null){
              return;
            }
            pingedServers.add(name);
            GameState gameState = ServerData.getInstance().getCurrentServer(serverInfoMap.get(name).getAddress().getAddress().getHostAddress(), serverInfoMap.get(name).getAddress().getPort()).getGameState();
            if(gameState == GameState.NOT_IN_GAME || gameState == GameState.LOBBY){
              if(serverInfoMap.get(name).getPlayers().size() < 12){
                foundServer[0] = true;
              }
            }
            if(pingedServers.size() == serverInfoMap.size()){
              if(!foundServer[0]){
                MallCore.getInstance().getRedis().sendMessage("newServer", gameStartEvent.getGameName() +":" + "1.0.0");
              }
            }

          });
        }
      }
    }
  }

  private static void handleServerReady(Event event){
    ServerReadyEvent serverReadyEvent = (ServerReadyEvent)event;
    if(Proxy.getInstance().getGamesQueue().get(serverReadyEvent.getGameName()) == null || Proxy.getInstance().getGamesQueue().get(serverReadyEvent.getGameName()).size() <= 0){
      return;
    }
    int count = 0;
    List<UUID> playersToRemove  = new ArrayList<>();
    for(Object obj : Proxy.getInstance().getGamesQueue().get(serverReadyEvent.getGameName())){
      if(count ==12){
        continue;
      }
      UUID uuid = (UUID) obj;
      Proxy.getInstance().getProxy().getPlayer(uuid).connect(Proxy.getInstance().getProxy().getServerInfo(serverReadyEvent.getServer().getNickname()));
      count++;
      playersToRemove.add(uuid);
    }
    Proxy.getInstance().getGamesQueue().get(serverReadyEvent.getGameName()).removeAll(playersToRemove);
  }

  private static void handleJoinGame(Event event){
    JoinGameEvent event1 = (JoinGameEvent)event;
    if(event1.getGameName().equalsIgnoreCase("laser")){
      Proxy.getInstance().getProxy().getPlayer(event1.getMallPlayer().getUuid()).connect(
          Proxy.getInstance().getProxy().getServerInfo("lasertest"));
    }
//    Proxy.getInstance().getGamesQueue().computeIfAbsent(event1.getGameName(), k -> new ArrayList());
//    for(String gameName : Proxy.getInstance().getGamesQueue().keySet()){
//      if(Proxy.getInstance().getGamesQueue().get(gameName).contains(event1.getMallPlayer().getUuid())){
//        if(gameName.equalsIgnoreCase(event1.getGameName())){
//          Messaging.sendMessage(event1.getMallPlayer(), "proxy.queue.already");
//          return;
//        }else{
//          Proxy.getInstance().getGamesQueue().get(gameName).remove(event1.getMallPlayer().getUuid());
//        }
//      }
//    }
//    if(Proxy.getInstance().getGamesQueue().get(event1.getGameName()).size() > 0){
//      Proxy.getInstance().getGamesQueue().get(event1.getGameName()).add(event1.getMallPlayer().getUuid());
//      Messaging.sendMessage(event1.getMallPlayer(), "proxy.queue.joined");
//      return;
//    }
//    if(Proxy.getInstance().getGamesQueue().get(event1.getGameName()).contains(event1.getMallPlayer().getUuid())){
//      Messaging.sendMessage(event1.getMallPlayer(), "proxy.queue.already");
//      return;
//    }
//
//    Map<String, ServerInfo> serverInfoMap = Proxy.getInstance().getProxy().getServers();
//
//    Map<String, ServerInfo> gamemodeServers = new HashMap<>();
//    for (String name : serverInfoMap.keySet()){
//      if(name.contains(event1.getGameName())){
//          gamemodeServers.put(name, serverInfoMap.get(name));
//      }
//    }
//    final ArrayList<ServerInfo> fullestServer = new ArrayList<>();
//    final int[] highestCount = {0};
//    final int[] serverCount = {0};
//    for(String laserName: gamemodeServers.keySet()){
//      Proxy.getInstance().getProxy().getServerInfo(laserName).ping(((serverPing, throwable) -> {
//        if(throwable != null){
//          serverCount[0]++;
//          if(serverCount[0] == gamemodeServers.size()){
//            if(fullestServer.size() == 0){
//              MallCore.getInstance().getRedis().sendMessage("newServer", event1.getGameName() +":" + "1.0.0");
//              Proxy.getInstance().getGamesQueue().get(event1.getGameName()).add(event1.getMallPlayer().getUuid());
//              Messaging.sendMessage(event1.getMallPlayer(), "proxy.queue.joined");
//            }else{
//              Proxy.getInstance().getProxy().getPlayer(event1.getMallPlayer().getUuid()).connect(
//                  fullestServer.get(0));
//            }
//          }
//          return;
//        }
//        if(serverPing.getPlayers().getOnline() < 12){
//          if(highestCount[0] < serverPing.getPlayers().getOnline()){
//            highestCount[0] = serverPing.getPlayers().getOnline();
//            if(fullestServer.size() > 0) {
//              fullestServer.remove(fullestServer.get(0));
//            }
//            fullestServer.add(serverInfoMap.get(laserName));
//          }
//        }
//        serverCount[0]++;
//        if(serverCount[0] == gamemodeServers.size()){
//          if(fullestServer.size() == 0){
//            MallCore.getInstance().getRedis().sendMessage("newServer", event1.getGameName() +":" + "1.0.0");
//            Proxy.getInstance().getGamesQueue().get(event1.getGameName()).add(event1.getMallPlayer().getUuid());
//            Messaging.sendMessage(event1.getMallPlayer(), "proxy.queue.joined");
//          }else{
//            Proxy.getInstance().getProxy().getPlayer(event1.getMallPlayer().getUuid()).connect(
//                fullestServer.get(0));
//          }
//        }
//      }));
//    }
  }
}
