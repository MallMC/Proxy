package info.mallmc.proxy.commands.tp;

import info.mallmc.proxy.util.Messaging;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;


public class HubCommand extends Command {

  public HubCommand() {
    super("hub");
  }

  @Override
  public void execute(CommandSender sender, String[] args) {
    if (!(sender instanceof ProxiedPlayer)) {
      Messaging.sendMessage(sender, "global.permissions.notAllowed");
      return;
    }
    ProxiedPlayer p = (ProxiedPlayer) sender;

    if (args.length == 0) {
      Messaging.sendMessage(sender, "proxy.command.hub.sent", 1);
      p.connect(ProxyServer.getInstance().getServerInfo("hub1"));
    } else {
      Messaging.sendMessage(sender, "proxy.command.usage");
    }
  }
}
