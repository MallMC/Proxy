package info.mallmc.proxy.commands.mod;

import info.mallmc.core.api.logs.LL;
import info.mallmc.core.api.logs.Log;
import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.core.api.bans.Offense;
import info.mallmc.core.api.player.PermissionSet;
import info.mallmc.core.database.BanDB;
import info.mallmc.core.database.PlayerData;
import info.mallmc.core.util.BanUtil;
import info.mallmc.proxy.Proxy;
import info.mallmc.proxy.util.Messaging;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class BanCommand extends Command {

  public BanCommand() {
    super("ban");
  }

  @Override
  public void execute(CommandSender sender, String[] args) {
    if (sender instanceof ProxiedPlayer
        && MallPlayer.getPlayer(((ProxiedPlayer) sender).getUniqueId()).getPermissionSet().getPower()
        < PermissionSet.STAFF.getPower()) {
      Messaging.sendMessage(sender, "global.permissions.notAllowed");
      return;
    }

    if (args.length == 2 && !args[0].equalsIgnoreCase("help")) {
        if (sender instanceof ProxiedPlayer) {
          PlayerData.getInstance().getPlayerByUsername(args[0]).whenComplete(((player, throwable) -> {
            if(MallPlayer.getPlayer(((ProxiedPlayer) sender).getUniqueId()).getPermissionSet().getPower() < player.getPermissionSet().getPower()){
              Messaging.sendMessage(sender, "proxy.command.ban.notAllowed");
            }else{
              Offense offense = Offense.getOffense(args[1]);

              if (offense == null) {
                Messaging.sendMessage(sender, "proxy.command.ban.noOffense");
                Messaging.sendMessage(sender,  "proxy.command.usage","/ban help for more information");
                return;
              }
              if (MallPlayer.getPlayer(((ProxiedPlayer) sender).getUniqueId()).getPermissionSet().getPower() < offense
                  .getMinimumPermissions().getPower()) {
                Messaging.sendMessage(sender, "proxy.command.ban.notAllowed");
                return;
              }
              BanDB.getInstance().hasBanExpired(player.getUuid()).whenComplete(((aBoolean, throwable1) -> {
                if(!aBoolean){
                  Messaging.sendMessage(sender, "proxy.command.ban.alreadyBanned");
                  return;
                }
                BanDB.getInstance().banPlayer(((ProxiedPlayer) sender).getUniqueId(),player.getUuid(), offense, "", false);
                try {
                  ProxiedPlayer target = ProxyServer.getInstance().getPlayer(args[0]);
                  target.disconnect(new TextComponent(TextComponent.fromLegacyText(BanUtil.getKickMessage(0, offense.getName()))));
                } catch (NullPointerException ex) {
                  Log.error(LL.ERROR, Proxy.getInstance().getMallCore().getServer().getNickname(), "Ban Player", "Null Pointer Exception", ex.getMessage());
                }
                Messaging.sendStaffAlertMessage("&c&l{0} &chas been banned for {1} by {2}", true, args[0], offense.getName(), sender.getName());
              }));
            }
          }));
        }

    } else if (args.length >= 1 && args[0].equalsIgnoreCase("help")) {
      Messaging.sendRawCustomMessage(sender, "&8---- &6Banning Offenses &8----");
      Messaging.sendRawCustomMessage(sender, "&8- &6Ban Evasion &8-");
      Messaging.sendRawCustomMessage(sender, "&8- &6Spam &8-");
    } else {
      Messaging.sendMessage(sender, "proxy.command.usage","/ban <player> <reason>");
    }
  }
}
