package info.mallmc.proxy.commands.mod;


import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.core.api.player.PermissionSet;
import info.mallmc.proxy.Proxy;
import info.mallmc.proxy.util.Messaging;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class LinkCommand extends Command {

    public LinkCommand() {
        super("link", null);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if(!(sender instanceof ProxiedPlayer)){
            Messaging.sendMessage(sender, "global.permissions.notAllowed");
            return;
        }
        ProxiedPlayer player = (ProxiedPlayer) sender;
        MallPlayer mallPlayer = MallPlayer.getPlayer(player.getUniqueId());
        if(mallPlayer.getPermissionSet().getPower() < PermissionSet.STAFF.getPower()){
            return;
        }
        if(args.length > 0 && args.length > 1){
            Messaging.sendMessage(sender, "proxy.command.usage", "/link <email address>");
            return;
        }
        DBCollection staffDB = Proxy.getInstance().getMallCore().getDatabase().getDatastore().getDB().getCollection("staff");
        if(staffDB.find(new BasicDBObject().append("uuid", player.getUniqueId().toString())) != null){
            Messaging.sendMessage(sender, "proxy.command.link.alreadyLinked");
            return;
        }
        BasicDBObject newStaff = new BasicDBObject();
        newStaff.append("uuid", player.getUniqueId().toString());
        newStaff.append("username", player.getName());
        newStaff.append("email", args[0]);
        staffDB.save(newStaff);
        Messaging.sendMessage(player, "proxy.command.link.linked");
    }
}
