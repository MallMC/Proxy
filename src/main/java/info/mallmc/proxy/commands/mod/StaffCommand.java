
package info.mallmc.proxy.commands.mod;


import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.core.api.player.PermissionSet;
import info.mallmc.proxy.util.Messaging;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class StaffCommand extends Command {

  public StaffCommand() {
    super("staff");
  }

  @Override
  public void execute(CommandSender sender, String[] args) {
    if (sender instanceof ProxiedPlayer
        && MallPlayer.getPlayer(((ProxiedPlayer) sender).getUniqueId()).getPermissionSet().getPower()
        < PermissionSet.STAFF.getPower()) {
      Messaging.sendMessage(sender, "global.permissions.notAllowed");
      return;
    }
    if (args.length < 1) {
      Messaging.sendMessage(sender, "proxy.commands.usage","/staff <message>");
      return;
    }
    StringBuilder message = new StringBuilder();
    for (String s : args) {
      message.append(s).append(" ");
    }
    if (sender instanceof ProxiedPlayer) {
      ProxiedPlayer player = (ProxiedPlayer) sender;
      MallPlayer mallPlayer = MallPlayer.getPlayer(player.getUniqueId());
      Messaging.sendStaffChatMessage(mallPlayer, message.toString(), true);
    }else {
      Messaging.sendMessage(sender, "proxy.command.onlyPlayers");
    }

  }

}
