package info.mallmc.proxy.commands.admin;

import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.core.api.player.PermissionSet;
import info.mallmc.core.database.PlayerData;
import info.mallmc.proxy.util.Messaging;
import java.sql.Timestamp;
import java.util.concurrent.TimeUnit;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class WhoisCommand extends Command {

  public WhoisCommand() {
    super("whois");
  }

  @Override
  public void execute(CommandSender sender, String[] args) {
    if (sender instanceof ProxiedPlayer
        &&
        MallPlayer.getPlayer(((ProxiedPlayer) sender).getUniqueId()).getPermissionSet().getPower()
            < PermissionSet.STAFF
            .getPower()) {
      Messaging.sendMessage(sender, "global.permissions.notAllowed");
      return;
    }
    if (args.length == 0) {
      Messaging.sendMessage(sender, "proxy.command.usage", "/whois <player>");
      return;
    }
    if (ProxyServer.getInstance().getPlayer(args[0]) == null) {
      PlayerData.getInstance().getPlayerByUsername(args[0])
          .whenComplete((mallPlayer, throwable) -> {
            if (mallPlayer == null) {
              Messaging.sendRawCustomMessage(sender, "&cThat player does not exist");
              return;
            }
            Messaging.sendRawCustomMessage(sender, "&6Information for {0} &c(Offline)",
                mallPlayer.getUsername());
            Messaging.sendRawCustomMessage(sender,
                "&6Rank: " + mallPlayer.getRank().getColor() + mallPlayer.getRank().getName());
            if (mallPlayer.getRank().getPermissions()
                .getPower() != mallPlayer.getPermissionSet().getPower()) {
              Messaging.sendRawCustomMessage(sender,
                  "&6Custom permissions: &e" + mallPlayer.getPermissionSet().name());
            }
            Messaging
                .sendRawCustomMessage(sender, "&6Last seen on: &e{0}", mallPlayer.getLastServer());
            Messaging
                .sendRawCustomMessage(sender, "&6Last known IP: &e{0}", mallPlayer.getLastIP());
            Messaging.sendRawCustomMessage(sender, "&6Currency: &e{0}",
                (Integer) mallPlayer.getCurrency());

            if (mallPlayer.isBanned()) {
              if (!mallPlayer.getBanExpiry().isEmpty()) {
                Messaging.sendRawCustomMessage(sender, "&6Banned until: &e" + new Timestamp(
                    Long.parseLong(mallPlayer.getBanExpiry()) * 1000).toString());
                Messaging
                    .sendRawCustomMessage(sender, "&6Ban reason: &e" + mallPlayer.getBanReason());
              }
            } else {
              Messaging.sendRawCustomMessage(sender, "&6Not banned.");
            }
            Messaging.sendRawCustomMessage(sender, "&6Ban points: &e" + mallPlayer.getBanPoints());
          });
    } else {
      ProxiedPlayer targetPlayer = ProxyServer.getInstance().getPlayer(args[0]);
      MallPlayer target = MallPlayer.getPlayer(targetPlayer.getUniqueId());
      long playSecs = target.getPlayTime();
      String playTime = "";
      if (TimeUnit.MILLISECONDS.toDays(playSecs) > 0) {
        long days = TimeUnit.MILLISECONDS.toDays(playSecs);
        playTime += days + "d ";
        playSecs -= (days * 60 * 60 * 24) * 1000;
      }
      if (TimeUnit.MILLISECONDS.toHours(playSecs) > 0) {
        long hours = TimeUnit.MILLISECONDS.toHours(playSecs);
        playTime += hours + "h ";
        playSecs -= (hours * 60 * 60) * 1000;
      }
      if (TimeUnit.MILLISECONDS.toMinutes(playSecs) > 0) {
        long mins = TimeUnit.MILLISECONDS.toMinutes(playSecs);
        playTime += mins + "m ";
        playSecs -= (mins * 60) * 1000;
      }
      if (TimeUnit.MILLISECONDS.toSeconds(playSecs) > 0) {
        long seconds = TimeUnit.MILLISECONDS.toSeconds(playSecs);
        playTime += seconds + "s";
      }
      Messaging
          .sendRawCustomMessage(sender, "&6Information for {0} &a(Online)", target.getUsername());
      Messaging.sendRawCustomMessage(sender,
          "&6Current server: &b" + targetPlayer.getServer().getInfo().getName());
      Messaging.sendRawCustomMessage(sender, "&6Play time: " + playTime);
      Messaging.sendRawCustomMessage(sender,
          "&6Rank: " + target.getRank().getColor() + target.getRank().getName());
      if (sender instanceof ProxiedPlayer
          &&
          MallPlayer.getPlayer(((ProxiedPlayer) sender).getUniqueId()).getPermissionSet().getPower()
              >= PermissionSet.ADMIN.getPower()) {
        if (target.getRank().getPermissions().getPower() != target.getPermissionSet().getPower()) {
          Messaging
              .sendRawCustomMessage(sender,
                  "&6Custom permissions: &e" + target.getPermissionSet().name());
        }
        if (target.getPermissionSet().getPower() < MallPlayer
            .getPlayer(((ProxiedPlayer) sender).getUniqueId()).getPermissionSet().getPower()) {
          Messaging.sendRawCustomMessage(sender,
              "&6IP: &e" + targetPlayer.getAddress().getAddress().getHostAddress());
        }
      } else if (!(sender instanceof ProxiedPlayer)) {
          Messaging
              .sendRawCustomMessage(sender,
                  "&6Custom permissions: &e" + target.getPermissionSet().name());
          Messaging.sendRawCustomMessage(sender,
              "&6IP: &e" + targetPlayer.getAddress().getAddress().getHostAddress());
      }
      Messaging.sendRawCustomMessage(sender, "&6Currency: &e" + target.getCurrency());
      Messaging.sendRawCustomMessage(sender, "&6Not banned.");
      Messaging.sendMessage(sender, "&6Ban points: &e" + target.getBanPoints());
    }
  }

}
