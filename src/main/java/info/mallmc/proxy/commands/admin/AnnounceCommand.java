package info.mallmc.proxy.commands.admin;


import info.mallmc.core.api.logs.Log;
import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.core.api.player.PermissionSet;
import info.mallmc.core.util.TextUtils;
import info.mallmc.proxy.util.Messaging;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 * @author Rushmead
 */
public class AnnounceCommand extends Command {

  public AnnounceCommand() {
    super("announce");
  }

  @Override
  public void execute(CommandSender sender, String[] args) {
    if (sender instanceof ProxiedPlayer
        && MallPlayer.getPlayer(((ProxiedPlayer) sender).getUniqueId()).getPermissionSet().getPower()
        < PermissionSet.ADMIN.getPower()) {
      Messaging.sendMessage(sender, "global.permissions.notAllowed");
      return;
    }
    if (args.length < 1) {
      Messaging.sendMessage(sender, "proxy.command.usage", "/announce <message>");
      return;
    }
    StringBuilder message = new StringBuilder();
    for (String msg : args) {
      message.append("&b").append(msg).append(" ");
    }
    Log.broadcast(sender.getName(),"Proxy", message.toString());
    Messaging.broadcastRawMessage("&a------------------------------------------------");
    Messaging.broadcastRawMessage(TextUtils.getCenteredMessage("&e&lANNOUNCEMENT&r&e:"));
    Messaging
        .broadcastRawMessage(TextUtils.getCenteredMessage(Messaging.colorizeMessageString(
            message.toString())));
    Messaging.broadcastRawMessage("&a------------------------------------------------");
  }

}
