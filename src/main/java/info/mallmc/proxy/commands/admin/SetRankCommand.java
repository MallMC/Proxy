package info.mallmc.proxy.commands.admin;

import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.core.api.player.PermissionSet;
import info.mallmc.core.api.player.Rank;
import info.mallmc.proxy.Proxy;
import info.mallmc.proxy.util.Messaging;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.TabExecutor;

public class SetRankCommand extends Command implements TabExecutor {

  public SetRankCommand() {
    super("setrank");
  }

  @Override
  public void execute(CommandSender sender, String[] args) {
    if (sender instanceof ProxiedPlayer
        && MallPlayer.getPlayer(((ProxiedPlayer) sender).getUniqueId()).getPermissionSet().getPower()
        < PermissionSet.ADMIN.getPower()) {
      Messaging.sendMessage(sender, "global.permissions.notAllowed");
      return;
    }
    if (args.length == 0 || args.length < 2) {
      Messaging.sendMessage(sender, "framework.command.setrank.usage");
      return;
    }
    ProxiedPlayer playerToChangeRank = Proxy.getInstance().getProxy().getPlayer(args[0]);
    if (playerToChangeRank == null) {
      Messaging.sendMessage(sender, "global.command.offlinePlayer");
      return;
    }
    try {
      MallPlayer mallPlayerToChangeRank = MallPlayer.getPlayer(playerToChangeRank.getUniqueId());
      Rank rankToChangeTo = Rank.getRankFromName(args[1], Rank.DEFAULT);
      if (mallPlayerToChangeRank.getPermissionSet().getPower() == mallPlayerToChangeRank.getRank()
          .getPermissions().getPower()) {
        mallPlayerToChangeRank.setPermissionSet(rankToChangeTo.getPermissions());
      }
      mallPlayerToChangeRank.setRank(rankToChangeTo);
      Messaging.sendMessage(sender, "framework.command.setrank.set", playerToChangeRank.getName(), rankToChangeTo.getName());
      Messaging.sendMessage(playerToChangeRank, "proxy.player.rankChange");
    } catch (IllegalArgumentException exception) {
      Messaging.sendMessage(sender, "framework.command.setrank.usage");
    }
  }


  @Override
  public Iterable<String> onTabComplete(CommandSender sender, String[] args) {
    final String lastArg = ( args.length > 0 ) ? args[args.length - 1].toLowerCase() : "";
    if (args.length == 1) {
      Collection<ProxiedPlayer> playerCollection = Proxy.getInstance().getProxy().matchPlayer(args[0]);
      List<String> stringList = new ArrayList<>();
      playerCollection.forEach((player) -> stringList.add(player.getDisplayName()));
      return stringList;
    } else if (args.length > 1) {
      List<Rank> ranks = new ArrayList<>();
      ranks.addAll(Arrays.asList(Rank.values()));
      return ranks.stream().filter(rank -> rank.name().startsWith(lastArg))
          .collect(Collectors.toList()).stream().map(Enum::name)
          .collect(Collectors.toList());
    } else {
      return null;
    }
  }
}
