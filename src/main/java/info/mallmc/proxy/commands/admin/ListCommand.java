package info.mallmc.proxy.commands.admin;


import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.core.api.player.PermissionSet;
import info.mallmc.core.database.PlayerData;
import info.mallmc.proxy.Proxy;
import info.mallmc.proxy.util.Messaging;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class ListCommand extends Command {

  public ListCommand() {
    super("plist");
  }

  @Override
  public void execute(CommandSender sender, String[] args) {
    if (sender instanceof ProxiedPlayer
        && MallPlayer.getPlayer(((ProxiedPlayer) sender).getUniqueId()).getPermissionSet().getPower()
        < PermissionSet.STAFF.getPower()) {
      Messaging.sendMessage(sender, "global.permissions.notAllowe");
      return;
    }
    if (args.length == 0) {
      Messaging.sendRawCustomMessage(sender, "&8&l&m---------------------------------------------");
      Messaging.sendRawCustomMessage(sender, "&8- &6&lTOTAL: &b{0} players",
          ProxyServer.getInstance().getOnlineCount());
      Messaging.sendRawCustomMessage(sender, "");
      for (String server : Proxy.getInstance().getProxy().getServers().keySet()) {
        String name = Proxy.getInstance().getProxy().getServers().get(server).getName();

        if (Proxy.getInstance().getProxy().getServers().get(server).getPlayers().isEmpty()) {
          Messaging.sendRawCustomMessage(sender, "&8- &6{0} &7(Empty)", name);
        } else {
          String s = "";
          if (Proxy.getInstance().getProxy().getServers().get(server).getPlayers().size() > 1) {
            s = "s";
          }
          if(sender instanceof ProxiedPlayer) {
            Messaging.sendRawClickableMessage((ProxiedPlayer) sender, "&8- &6{0} &b({1} player{2})",
                "/plist " + name,
                name, Proxy.getInstance().getProxy().getServers().get(server).getPlayers().size(),
                s);
          }else{

            Messaging.sendRawCustomMessage(sender, "&8- &6{0} &b({1} player{2})",
                "/plist " + name,
                name, Proxy.getInstance().getProxy().getServers().get(server).getPlayers().size(),
                s);
          }
        }
      }
      Messaging.sendRawCustomMessage(sender, "&8&l&m---------------------------------------------");
    } else if (args.length == 1) {
      ServerInfo server = Proxy.getInstance().getProxy().getServerInfo(args[0]);
      if (server == null) {
        Messaging.sendMessage(sender, "&cSomething went wrong!");
        return;
      }
      StringBuilder stringBuilder = new StringBuilder();
      for (ProxiedPlayer p : server.getPlayers()) {
        if (stringBuilder.toString().isEmpty()) {
          PlayerData.getInstance().getRankFromDatabase(p.getUniqueId()).whenComplete(((rank, throwable) -> stringBuilder.append(rank).append(p.getName())));
        } else {
          PlayerData.getInstance().getRankFromDatabase(p.getUniqueId()).whenComplete(((rank, throwable) -> stringBuilder.append("&8, ").append(rank).append(p.getName())));
        }

      }
      Messaging.sendRawCustomMessage(sender, "&8- &6{0} &b({1})&8: {2}", server.getName(),
          server.getPlayers().size(), stringBuilder.toString());

    }

  }

}
