package info.mallmc.proxy.commands.admin;

import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.core.api.player.PermissionSet;
import info.mallmc.proxy.Proxy;
import info.mallmc.proxy.util.Messaging;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class TogglePublic extends Command {

  public TogglePublic() {
    super("public");
  }

  @Override
  public void execute(CommandSender sender, String[] args) {
    if (sender instanceof ProxiedPlayer) {
      if (MallPlayer.getPlayer(((ProxiedPlayer) sender).getUniqueId()).getPermissionSet().getPower()
          < PermissionSet.ADMIN.getPower()) {
        Messaging.sendMessage(sender, "global.permissions.notAllowed");
        return;
      }
    }
    if (Proxy.getInstance().isPublicMode()) {
      Proxy.getInstance().setPublicMode(false);
      Messaging.sendMessage(sender, "proxy.command.public.off");
    } else {
      Proxy.getInstance().setPublicMode(true);
      Messaging.sendMessage(sender, "proxy.command.public.on");
    }
  }

}
