package info.mallmc.proxy.commands.user;

import info.mallmc.core.api.ChatChannel;
import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.proxy.util.Messaging;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class ChannelCommand extends Command {

  public ChannelCommand() {
    super("channel", null, "c");
  }

  @Override
  public void execute(CommandSender sender, String[] args) {
    if (!(sender instanceof ProxiedPlayer)) {
      Messaging.sendMessage(sender, "proxy.command.usage");
      return;
    }
    ProxiedPlayer player = (ProxiedPlayer) sender;
    MallPlayer mallPlayer = MallPlayer.getPlayer(player.getUniqueId());
    if(args.length == 0){
      if(mallPlayer.getPermissionSet().getPower() >= ChatChannel.STAFF.getPermissionSet().getPower()){
        Messaging.sendMessage(sender, "proxy.command.usage","/channel <public/staff>");
        return;
      }
      Messaging.sendMessage(sender, "proxy.command.usage","/channel <public>");
      return;
    }
    if(mallPlayer.getChatChannel().getChannelName().equalsIgnoreCase(args[0])){
      Messaging.sendMessage(sender, "proxy.command.channel.alreadyChannel");
      return;
    }
    switch(args[0]){
      case "public":
        mallPlayer.setChatChannel(ChatChannel.ALL);
        Messaging.sendMessage(sender, "proxy.command.channel.channelChanged", args[0]);
        break;
      case "staff":
        if(mallPlayer.getPermissionSet().getPower() >= ChatChannel.STAFF.getPermissionSet().getPower()){
          mallPlayer.setChatChannel(ChatChannel.STAFF);
          Messaging.sendMessage(sender, "proxy.command.channel.channelChanged", args[0]);
          break;
        }
        break;
      default:
        if(mallPlayer.getPermissionSet().getPower() >= ChatChannel.STAFF.getPermissionSet().getPower()){
          Messaging.sendMessage(sender, "proxy.command.usage","/channel <public/staff>");
          break;
        }
        Messaging.sendMessage(sender, "proxy.command.usage","/channel <public>");
        break;
    }
  }
}
