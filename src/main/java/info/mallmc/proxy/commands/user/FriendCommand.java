package info.mallmc.proxy.commands.user;


import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.core.database.PlayerData;
import info.mallmc.proxy.util.Messaging;
import java.util.UUID;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class FriendCommand extends Command {

  public FriendCommand() {
    super("friend", null, "friends");
  }

  @Override
  public void execute(CommandSender sender, String[] args) {
    if (!(sender instanceof ProxiedPlayer)) {
      Messaging.sendMessage(sender, "proxy.command.usage");
      return;
    }
    ProxiedPlayer player = (ProxiedPlayer) sender;
    MallPlayer mallPlayer = MallPlayer.getPlayer(player.getUniqueId());
    if (args.length == 0) {
      Messaging.sendMessage(sender, "proxy.command.usage",
          "/friend <add/remove/list/deny/accept/disable>");
      return;
    }
    String subCommand = args[0];
    switch (subCommand) {
      case "add":
        if (args.length > 2 || args.length < 2) {
          Messaging.sendMessage(sender, "proxy.command.usage",
              "/friend add <username> - Send friend request to player");
          return;
        }
        String playerUsername = args[1];
        PlayerData.getInstance().getUUIDByUsername(playerUsername.toLowerCase())
            .whenComplete(((playerUUID, throwable) -> {
              if (playerUUID == null) {
                Messaging.sendMessage(player,
                    MallPlayer.FriendResult.PLAYER_DOES_NOT_EXIST.getMessage());
                return;
              }
              mallPlayer.addFriend(playerUUID).whenComplete((friendResult, throwable1) -> Messaging.sendMessage(player, friendResult.getMessage()));
            }));

      case "remove":
        if (args.length > 2 || args.length < 2) {
          Messaging.sendMessage(sender, "proxy.command.usage",
              "/friend remove <username> - Remove friend");
          return;
        }
        String playerUser = args[1];
        PlayerData.getInstance().getUUIDByUsername(playerUser.toLowerCase())
            .whenComplete((playerUUID, throwable) -> {

              if (playerUUID == null) {
                Messaging.sendMessage(player, MallPlayer.FriendResult.PLAYER_DOES_NOT_EXIST.getMessage());
                return;
              }
              mallPlayer.removeFriend(playerUUID).whenComplete((friendResult, throwable1) -> Messaging.sendMessage(player, friendResult.getMessage()));
            });
        break;
      case "list":
        Messaging
            .sendMessage(sender, "proxy.commands.friends.list", mallPlayer.getFriends().size());
        for (UUID friend : mallPlayer.getFriends()) {
          if (MallPlayer.isOnline(friend)) {
            MallPlayer friendPlayer = MallPlayer.getPlayer(friend);
            Messaging.sendRawCustomMessage(player,
                "&6 - &r{0}{1} &r&6- &l&aOnline &r&6- Last seen: &l{2}",
                friendPlayer.getRank().getColor(), friendPlayer.getUsername(),
                friendPlayer.getLastServer());
          } else {
            PlayerData.getInstance().getPlayerByUUID(friend).whenComplete((offlineFriend, throwable) -> Messaging.sendRawCustomMessage(player,
                "&6 - &r{0}{1} &r&6- &l&cOffline &r&6- Last seen: &l{2}",
                offlineFriend.getRank().getColor(), offlineFriend.getUsername(),
                offlineFriend.getLastServer()));
          }
        }
        Messaging.sendMessage(sender, "proxy.command.friends.requests",
            mallPlayer.getFriendRequests().size());
        for (UUID friendRequest : mallPlayer.getFriendRequests()) {
          MallPlayer friendPlayer = MallPlayer.getPlayer(friendRequest);
          Messaging.sendRawClickableMessage(player, "&6 - &r{0}{1} - &r&6Click to accept!",
              "/friend accept " + friendPlayer.getUsername(), friendPlayer.getRank().getColor(),
              friendPlayer.getUsername(), friendPlayer.getLastServer());
        }
        break;
      case "deny":
        if (args.length > 2 || args.length < 2) {
          Messaging.sendMessage(sender, "proxy.command.usage",
              "/friend deny <username> - Deny friend request from player");
          return;
        }
        String playerUser2 = args[1];
        PlayerData.getInstance().getUUIDByUsername(playerUser2).whenComplete((playerUUID4, throwable) -> {
          if (playerUUID4 == null) {
            Messaging.sendMessage(player, MallPlayer.FriendResult.PLAYER_DOES_NOT_EXIST.getMessage());
          }
          mallPlayer.denyFriendRequest(playerUUID4).whenComplete((result4, throwable1) -> Messaging.sendMessage(player, result4.getMessage()));
        });
        break;
      case "accept":
        if (args.length > 2 || args.length < 2) {
          Messaging.sendMessage(sender, "proxy.command.usage",
              "/friend deny <username> - Deny friend request from player");
          return;
        }
        String playerUser3 = args[1];
        PlayerData.getInstance().getUUIDByUsername(playerUser3).whenComplete((playerUUID4, throwable) -> {
          if (playerUUID4 == null) {
            Messaging.sendMessage(player, MallPlayer.FriendResult.PLAYER_DOES_NOT_EXIST.getMessage());
          }
           mallPlayer.acceptFriendRequest(playerUUID4).whenComplete((result4, throwable1) -> Messaging.sendMessage(player, result4.getMessage()));
        });
        break;
      case "disable":
        if (mallPlayer.isFriendRequestsEnabled()) {
          mallPlayer.setFriendRequestsEnabled(false);
          Messaging.sendMessage(player, "proxy.command.friends.toggle.on");
          break;
        } else {
          mallPlayer.setFriendRequestsEnabled(true);
          Messaging.sendMessage(player, "proxy.command.friends.toggle.on");
          break;
        }
      default:
        Messaging.sendMessage(sender, "proxy.command.usage",
            "/friend <add/remove/list/deny/accept/disable>");
        break;
    }
  }
}
