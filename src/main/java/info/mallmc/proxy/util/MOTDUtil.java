package info.mallmc.proxy.util;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import info.mallmc.core.MallCore;
import info.mallmc.core.util.DefaultFontInfo;
import info.mallmc.proxy.Proxy;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.TextComponent;

public class MOTDUtil {

  private final static int CENTER_PX = 127;
  private final static int MAX_PX = 240;

  private static String sendCenteredMessage(String message){
    message = ChatColor.translateAlternateColorCodes('&', message);
    int messagePxSize = 0;
    boolean previousCode = false;
    boolean isBold = false;
    int charIndex = 0;
    int lastSpaceIndex = 0;
    String toSendAfter = null;
    String recentColorCode = "";
    for(char c : message.toCharArray()){
      if(c == '.'){ //Was ?
        previousCode = true;
        continue;
      }else if(previousCode){
        previousCode = false;
        recentColorCode = "." + c; //Was
        if(c == 'l' || c == 'L'){
          isBold = true;
          continue;
        } else {
          isBold = false;
        }
      }else if(c == ' ') {
        lastSpaceIndex = charIndex;
      } else{
        DefaultFontInfo dFI = DefaultFontInfo.getDefaultFontInfo(c);
        messagePxSize += isBold ? dFI.getBoldLength() : dFI.getLength();
        messagePxSize++;
      }
      if(messagePxSize >= MAX_PX){
        toSendAfter = recentColorCode + message.substring(lastSpaceIndex + 1, message.length());
        message = message.substring(0, lastSpaceIndex + 1);
        break;
      }
      charIndex++;
    }
    int halvedMessageSize = messagePxSize / 2;
    int toCompensate = CENTER_PX - halvedMessageSize;
    int spaceLength = DefaultFontInfo.SPACE.getLength() + 1;
    int compensated = 0;
    StringBuilder sb = new StringBuilder();
    while(compensated < toCompensate){
      sb.append(" ");
      compensated += spaceLength;
    }
    if(toSendAfter != null) {
      sendCenteredMessage(toSendAfter);
    }
    return sb.toString() + message;
  }

  public static TextComponent getMOTD(){
    if (Proxy.getInstance().getMOTD() == null) {
      DBObject motdFound = MallCore.getInstance().getDatabase().getDatastore().getDB().getCollection("motd").findOne(new BasicDBObject("createdAt", -1));
      if (motdFound == null) {
        Proxy.getInstance().setMOTD("MallMC");
      } else {
        Proxy.getInstance().setMOTD(
            (String) motdFound.get("motd"));
      }
    }
    String theMOTD = Proxy.getInstance().getMOTD();
    String MOTD;
    String[] theMOTDs = theMOTD.split("\n");
    theMOTDs[0] = theMOTDs[0].replace("\n", "");
    MOTD = sendCenteredMessage(theMOTDs[0]) + "\n";
    if(theMOTDs.length > 1){
      theMOTDs[1] = theMOTDs[1].replace("\n", "");
      MOTD = MOTD + sendCenteredMessage(theMOTDs[1]);
    }
    return Messaging.colorizeMessage(MOTD);
  }

}
