package info.mallmc.proxy.util;

import info.mallmc.core.MallCore;
import info.mallmc.core.api.logs.LL;
import info.mallmc.core.api.logs.Log;
import info.mallmc.core.api.player.EnumLanguageIdentifier;
import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.core.api.player.PermissionSet;
import info.mallmc.core.events.EventType;
import info.mallmc.core.events.events.discord.SendMessageToDiscordEvent;
import info.mallmc.core.util.TextUtils;
import info.mallmc.core.util.discord.DiscordMessage;
import info.mallmc.core.util.discord.DiscordMessageType;
import info.mallmc.proxy.Proxy;
import java.awt.Color;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;

/**
 * @author Rushmead
 */
public class Messaging {


  public final static EnumLanguageIdentifier COMMAND_SENDER_LANGUAGE_IDENTIFER = EnumLanguageIdentifier.EN;


  public static String getPrefix(EnumLanguageIdentifier enumLanguageIdentifier) {
    return colorizeMessageString(getMessage(enumLanguageIdentifier, "global.prefix"));
  }

  /**
   * @param key The name of the message
   * @param replacements Things to replace in the message
   * @return The message
   */
  private static String getMessage(EnumLanguageIdentifier enumLanguageIdentifier, String key,
      Object... replacements) {
    String message = MallCore.getInstance().getLanguageMap().get(enumLanguageIdentifier)
        .getMessage(key);
    if (message == null) {
      Log.error(LL.ERROR, "bungee", "Message",
          "Key is null", key + " could not be found");
      return "";
    }
    return TextUtils.replace(message, replacements);
  }

  public static String colorizeMessageString(String message) {
    if (message == null || message.isEmpty()) {
      return "";
    }
    return ChatColor.translateAlternateColorCodes('&', message);
  }

  /**
   * @return Colorized message
   */
  public static TextComponent colorizeMessage(String message) {
    if (message == null || message.isEmpty()) {
      return new TextComponent("");
    }
    return new TextComponent(TextComponent.fromLegacyText(ChatColor.translateAlternateColorCodes('&', message)));
  }

  /**
   * @param commandSender consol
   * @param key The predefined message key
   * @param replacements The replacements for the message
   */
  public static void sendMessage(CommandSender commandSender, String key,
      Object... replacements) {
    if(commandSender instanceof  ProxiedPlayer){
      MallPlayer mallPlayer = MallPlayer.getPlayer(((ProxiedPlayer) commandSender).getUniqueId());
      commandSender.sendMessage(colorizeMessage(getPrefix(mallPlayer.getLanguageIdentifier()) + getMessage(mallPlayer.getLanguageIdentifier(), key, replacements)));
    }else{
      commandSender.sendMessage(colorizeMessage(getPrefix(COMMAND_SENDER_LANGUAGE_IDENTIFER) + getMessage(COMMAND_SENDER_LANGUAGE_IDENTIFER, key, replacements)));
    }
  }

  /**
   * Sends a predefined message to a Player
   *
   * @param player The player to send it to
   * @param key The predefined message key
   * @param replacements The replacements for the message
   */
  private static void sendMessage(MallPlayer player, String key, Object... replacements) {
    Proxy.getInstance().getProxy().getPlayer(player.getUuid()).sendMessage(colorizeMessage(
        getPrefix(player.getLanguageIdentifier()) + getMessage(player.getLanguageIdentifier(), key,
            replacements)));
  }

  private static void sendRawMessage(MallPlayer player, String message, Object... replacements){
    Proxy.getInstance().getProxy().getPlayer(player.getUuid()).sendMessage(colorizeMessage(TextUtils.replace(message, replacements)));
  }

  /**
   * Broadcasts a predefined message to the server
   *
   * @param key Predefined message key
   * @param replacements The replacements for the message
   */
  public static void broadcastMessage(String key, Object... replacements) {
    for (MallPlayer mallPlayer : MallPlayer.getPlayers().values()) {
      sendMessage(mallPlayer, key, replacements);
    }
    sendMessage(Proxy.getInstance().getProxy().getConsole(), key, replacements);
  }

  /**
   * Broadcasts a predefined message to the server
   *
   * @param message Custom message
   * @param replacements The replacements for the message
   */
  public static void broadcastRawMessage(String message, Object... replacements) {
    for (MallPlayer mallPlayer : MallPlayer.getPlayers().values()) {
      sendRawMessage(mallPlayer, message, replacements);
    }
    sendRawMessage(Proxy.getInstance().getProxy().getConsole(), message, replacements);
  }

  /**
   * @param commandSender consol
   * @param key The predefined message key
   * @param replacements The replacements for the message
   */
  private static void sendRawMessage(CommandSender commandSender, String key,
      Object... replacements) {
    if(commandSender instanceof  ProxiedPlayer){
      MallPlayer mallPlayer = MallPlayer.getPlayer(((ProxiedPlayer) commandSender).getUniqueId());
      commandSender.sendMessage(
          colorizeMessage(getMessage(mallPlayer.getLanguageIdentifier(), key, replacements)));
    }else{
      commandSender.sendMessage(
          colorizeMessage(getMessage(COMMAND_SENDER_LANGUAGE_IDENTIFER, key, replacements)));
    }
  }

  /**
   * @param commandSender consol
   * @param message The predefined message key
   * @param replacements The replacements for the message
   */
  public static void sendRawCustomMessage(CommandSender commandSender, String message,
      Object... replacements) {
      commandSender.sendMessage(
          colorizeMessage(TextUtils.replace(message, replacements)));
  }

  /**
   * Send TextComponent to all staff
   *
   * @param message The message to send to all staff
   */
  private static void sendToAllStaff(String message) {
    for (ServerInfo server : ProxyServer.getInstance().getServers().values()) {
      for (ProxiedPlayer ps : server.getPlayers()) {
        try {
          if (MallPlayer.getPlayer(ps.getUniqueId()).getPermissionSet().getPower() >= PermissionSet.STAFF.getPower()) {
            ps.sendMessage(message);
          }
        } catch (NullPointerException e) {
          Log.error(LL.ERROR, Proxy.getInstance().getMallCore().getServer().getNickname(), "Staff Message", "Null Pointer Exception", e.getMessage());
        }
      }
    }
  }
  private static void sendStaffChatMessageToDiscord(MallPlayer mallPlayer, String message)
  {
    DiscordMessage discordMessage = new DiscordMessage(mallPlayer.getUsername());
    discordMessage.setAuthorIconUrl("https://minotar.net/avatar/" + mallPlayer.getUsername() + "/100.png");
    discordMessage.setDescription(message);
    SendMessageToDiscordEvent event1 = new SendMessageToDiscordEvent(discordMessage, DiscordMessageType.CHAT, mallPlayer);
    info.mallmc.core.events.EventHandler.triggerEvent(EventType.MESSAGE_TO_DISCORD_EVENT, event1);
  }

  public static void sendStaffChatMessage(MallPlayer mallPlayer, String message, boolean discord) {

    ProxiedPlayer proxiedPlayer = ProxyServer.getInstance().getPlayer(mallPlayer.getUuid());
    String message2 = colorizeMessage("&5[Staff&6-&3" + proxiedPlayer.getServer().getInfo().getName() + "]&3 " + mallPlayer.getUsername() + "&3:&r ") + message;
    sendToAllStaff(message2);
    if(discord) {
      sendStaffChatMessageToDiscord(mallPlayer, message);
    }
  }
  public static void sendStaffChatMessage(String message) {

    String message2 = colorizeMessage("&5[Staff&6-&3Discord]&3:&r ") + message;
    sendToAllStaff(message2);
  }

  private static void sendStaffAlertMessageToDiscord(String message)
  {
    DiscordMessage discordMessage = new DiscordMessage("Alert");
    discordMessage.setAuthorIconUrl("https://mallmc.info/static/img/logo.png");
    discordMessage.setDescription(message);
    discordMessage.setColor(Color.RED);
    SendMessageToDiscordEvent event1 = new SendMessageToDiscordEvent(discordMessage, DiscordMessageType.ALERT, null);
    info.mallmc.core.events.EventHandler.triggerEvent(EventType.MESSAGE_TO_DISCORD_EVENT, event1);
  }

  public static void sendStaffAlertMessage(String message, boolean discord, Object... replace) {

    String message2 = colorizeMessage("&5[Staff&6-&3Alert]&3: ") + TextUtils.replace(message, replace);
    sendToAllStaff(message2);
    if(discord) {
      sendStaffAlertMessageToDiscord(message);
    }
  }



//  /**
//   * Sends the player a hovering message
//   *
//   * @param player The player to send it to
//   * @param message The message to send
//   * @param hover The hovering message
//   * @param replacements The replacements for the message
//   */
//  public static void sendHoveringMessage(ProxiedPlayer player, String message, String hover,
//      Object... replacements) {
//    TextComponent messageComponent = new TextComponent(
//        TextComponent.fromLegacyText(replace(getPrefix() + message, replacements)));
//    messageComponent.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(
//        Arrays.toString(TextComponent.fromLegacyText(replace(message, replacements)))).create()));
//    player.sendMessage(messageComponent);
//  }
//
  /**
   * Send the player a clickable message
   *
   * @param player The player to send it to
   * @param key The key of the message to send
   * @param command The command to execute upon clicking it
   * @param replacements The replacements for the message
   */
  public static void sendClickableMessage(ProxiedPlayer player, String key, String command,
      Object... replacements) {
    MallPlayer mallPlayer = MallPlayer.getPlayer(player.getUniqueId());
    TextComponent messageComponent = new TextComponent(
        TextComponent.fromLegacyText(getPrefix(mallPlayer.getLanguageIdentifier()) + getMessage(mallPlayer.getLanguageIdentifier(), key, replacements)));
    messageComponent.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, command));
    player.sendMessage(messageComponent);
  }

  /**
   * Send the player a clickable message
   *
   * @param player The player to send it to
   * @param message The key of the message to send
   * @param command The command to execute upon clicking it
   * @param replacements The replacements for the message
   */
  public static void sendRawClickableMessage(ProxiedPlayer player, String message, String command,
      Object... replacements) {
    TextComponent messageComponent = new TextComponent(
        TextComponent.fromLegacyText(TextUtils.replace(message, replacements)));
    messageComponent.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, command));
    player.sendMessage(messageComponent);
  }
//
//  /**
//   * Send a CommandSender a clickable message
//   *
//   * @param player The CommandSender to send it to
//   * @param message The message to send
//   * @param command The command to execute on click
//   * @param replacements The objects to replace with in the message
//   */
//  public static void sendClickableMessage(CommandSender player, String message, String command,
//      Object... replacements) {
//    TextComponent messageComponent = new TextComponent(
//        TextComponent.fromLegacyText(getPrefix() + replace(message, replacements)));
//    messageComponent.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, command));
//    player.sendMessage(messageComponent);
//  }
//
//  /**
//   * Send the player a clickable and hoverable message
//   *
//   * @param player The player to send it to
//   * @param message The message to send
//   * @param hover The hover message
//   * @param command the command to run on click
//   * @param replacements The replacements for the message
//   */
//  public static void sendClickableAndHoveringMessage(ProxiedPlayer player, String message,
//      String hover, String command, Object... replacements) {
//    TextComponent messageComponent = new TextComponent(
//        TextComponent.fromLegacyText(getPrefix() + replace(message, replacements)));
//    messageComponent.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, command));
//    messageComponent.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(
//        Arrays.toString(TextComponent.fromLegacyText(replace(message, replacements)))).create()));
//    player.sendMessage(messageComponent);
//  }
//
//  /**
//   * Sends the player a raw(no prefix) hovering message
//   *
//   * @param player The player to send it to
//   * @param message The message to send
//   * @param hover The hovering message
//   * @param replacements The replacements for the message
//   */
//  public static void sendRawHoveringMessage(ProxiedPlayer player, String message, String hover,
//      Object... replacements) {
//    TextComponent messageComponent = new TextComponent(
//        TextComponent.fromLegacyText(replace(message, replacements)));
//    messageComponent.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(
//        Arrays.toString(TextComponent.fromLegacyText(replace(message, replacements)))).create()));
//    player.sendMessage(messageComponent);
//  }
//
//  /**
//   * Send the player a raw(No Prefix) clickable message
//   *
//   * @param player The player to send it to
//   * @param message The message to send
//   * @param command The command to execute upon clicking it
//   * @param replacements The replacements for the message
//   */
//  public static void sendRawClickableMessage(ProxiedPlayer player, String message, String command,
//      Object... replacements) {
//    TextComponent messageComponent = new TextComponent(
//        TextComponent.fromLegacyText(replace(message, replacements)));
//    messageComponent.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, command));
//    player.sendMessage(messageComponent);
//  }
//
//  /**
//   * Send the CommandSender a message with no prefix (RAW)
//   *
//   * @param player The CommandSender to send it to
//   * @param message The message to send
//   * @param command The command to execute
//   * @param replacements The objects to replace with in the message
//   */
//  public static void sendRawClickableMessage(CommandSender player, String message, String command,
//      Object... replacements) {
//    TextComponent messageComponent = new TextComponent(
//        TextComponent.fromLegacyText(replace(message, replacements)));
//    messageComponent.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, command));
//    player.sendMessage(messageComponent);
//  }
//
//  /**
//   * Send the player a raw(No Prefix) clickable and hoverable message
//   *
//   * @param player The player to send it to
//   * @param message The message to send
//   * @param hover The hover message
//   * @param command the command to run on click
//   * @param replacements The replacements for the message
//   */
//  public static void sendRawClickableAndHoveringMessage(ProxiedPlayer player, String message,
//      String hover, String command, Object... replacements) {
//    TextComponent messageComponent = new TextComponent(
//        TextComponent.fromLegacyText(replace(message, replacements)));
//    messageComponent.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, command));
//    messageComponent.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(
//        Arrays.toString(TextComponent.fromLegacyText(replace(message, replacements)))).create()));
//    player.sendMessage(messageComponent);
//  }
}
