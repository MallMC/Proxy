package info.mallmc.proxy;

import info.mallmc.core.MallCore;
import info.mallmc.core.api.Server;
import info.mallmc.proxy.commands.admin.AnnounceCommand;
import info.mallmc.proxy.commands.admin.ListCommand;
import info.mallmc.proxy.commands.admin.SetRankCommand;
import info.mallmc.proxy.commands.admin.TogglePublic;
import info.mallmc.proxy.commands.admin.WhoisCommand;
import info.mallmc.proxy.commands.mod.BanCommand;
import info.mallmc.proxy.commands.mod.LinkCommand;
import info.mallmc.proxy.commands.mod.StaffCommand;
import info.mallmc.proxy.commands.tp.HubCommand;
import info.mallmc.proxy.commands.user.ChannelCommand;
import info.mallmc.proxy.commands.user.FriendCommand;
import info.mallmc.proxy.events.CoreEvents;
import info.mallmc.proxy.events.PlayerEvents;
import info.mallmc.proxy.util.Constants;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.md_5.bungee.api.config.ListenerInfo;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.mapping.DefaultCreator;
import org.mongodb.morphia.query.Query;

@SuppressWarnings("ResultOfMethodCallIgnored")
public class Proxy extends Plugin {

  private static Proxy instance;

  public static Proxy getInstance() {
    return instance;
  }

  private MallCore mallCore;
  private Configuration configuration;
//  private SlackManager slackManager;
  private boolean publicMode = Constants.IS_PUBLIC;
  private Map<String, List<UUID>> gamesQueue;
  private String MOTD;
  private int maxPlayers;

  public MallCore getMallCore() {
    return mallCore;
  }

  @Override
  public void onEnable() {
    gamesQueue = new HashMap<>();
    instance = this;
    createConfig();
    try {
      configuration = ConfigurationProvider.getProvider(YamlConfiguration.class)
          .load(new File(getDataFolder(), "config.yml"));
    } catch (IOException ex) {
      getLogger().log(Level.SEVERE, "Could not get config! ERROR ERROR!");
    }
    String ip = getConfig().getString("database.ip");
    int port = getConfig().getInt("database.port");
    String username = getConfig().getString("database.username");
    String password = getConfig().getString("database.password");
    String redisIP = getConfig().getString("redis.ip");
    int redisPort = getConfig().getInt("redis.port");
    String redisPassword = getConfig().getString("redis.password");
    ListenerInfo listenerInfo = getProxy().getConfigurationAdapter().getListeners().iterator().next();
    this.mallCore = new MallCore(listenerInfo.getHost().getAddress().getHostAddress(), listenerInfo.getHost().getPort());
    this.mallCore.connect(ip, port, username, password);
    this.mallCore.redisConnect(redisIP, redisPassword, redisPort);
    this.mallCore.getDatabase().getMorphia().getMapper().getOptions().setObjectFactory(new DefaultCreator() {
      @Override
      protected ClassLoader getClassLoaderForClass() {
        return this.getClass().getClassLoader();
      }
    });
    this.mallCore.getDatabase().setupMorphia();
    this.mallCore.getDatabase().setupLanguage();
    getProxy().getPluginManager().registerListener(this, new PlayerEvents());
    getProxy().getPluginManager().registerCommand(this, new AnnounceCommand());
    getProxy().getPluginManager().registerCommand(this, new ListCommand());
    getProxy().getPluginManager().registerCommand(this, new TogglePublic());
    getProxy().getPluginManager().registerCommand(this, new WhoisCommand());
    getProxy().getPluginManager().registerCommand(this, new BanCommand());
    getProxy().getPluginManager().registerCommand(this, new LinkCommand());
    getProxy().getPluginManager().registerCommand(this, new StaffCommand());
    getProxy().getPluginManager().registerCommand(this, new HubCommand());
    getProxy().getPluginManager().registerCommand(this, new ChannelCommand());
    getProxy().getPluginManager().registerCommand(this, new FriendCommand());
    getProxy().getPluginManager().registerCommand(this, new SetRankCommand());
//    slackManager = new SlackManager(webHookURL, slackPort, token);
//    slackManager.load();
    CoreEvents.handleCoreEvents();
    Datastore datastore = mallCore.getDatabase().getDatastore();
    Query<Server> serverQuery = datastore.createQuery(Server.class);
    final List<Server> servers = serverQuery.asList();
    for(Server server : servers){
      if(getProxy().getServerInfo(server.getNickname()) == null){
        ServerInfo serverInfo = getProxy().constructServerInfo(server.getNickname(), new InetSocketAddress(server.getIp(), server.getPort()),"lolhax", false);
        getProxy().getServers().put(server.getNickname(), serverInfo);
      }
    }
    Proxy.getInstance().getProxy().getServers().values().forEach((server) -> server.ping((serverPing, error) ->{
      if(serverPing != null){maxPlayers += serverPing.getPlayers().getMax(); }}));
  }

  public String getMOTD() {
    return MOTD;
  }

  public void setMOTD(String MOTD) {
    this.MOTD = MOTD;
  }

  public int getMaxPlayers() {
    return maxPlayers;
  }

  public Map<String, List<UUID>> getGamesQueue() {
    return gamesQueue;
  }

  @Override
  public void onDisable() {
    mallCore.disable();
  }

//  public SlackManager getSlackManager() {
//    return slackManager;
//  }

  private Configuration getConfig() {
    return configuration;
  }

  public boolean isPublicMode() {
    return publicMode;
  }

  public void setPublicMode(boolean publicMode) {
    this.publicMode = publicMode;
  }

  private void createConfig() {
    if (!getDataFolder().exists()) {
      getDataFolder().mkdir();
    }

    File file = new File(getDataFolder(), "config.yml");
    try {
      file.createNewFile();
    } catch (IOException ex) {
      Logger.getLogger(Proxy.class.getName()).log(Level.SEVERE, null, ex);
    }
    if (!file.exists()) {
      try (InputStream in = getResourceAsStream("config.yml")) {
        Files.copy(in, file.toPath());
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }
}
